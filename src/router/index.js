import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from "@/views/AboutView.vue";
import ItemDetailView from "@/views/ItemDetailView.vue";

export const ITEM_NAME = 'about'

const router = createRouter({
  history: createWebHistory(), // 🤡
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/item',
      name: ITEM_NAME,
      component: AboutView
    },
    {
      path: '/about/:itemIndex',
      name: 'item-detail',
      component: ItemDetailView
    }
  ]
})

export default router
